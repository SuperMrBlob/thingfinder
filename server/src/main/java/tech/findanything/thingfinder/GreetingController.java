package tech.findanything.thingfinder;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.util.HtmlUtils;

import tech.findanything.thingfinder.data.DataService;
import tech.findanything.thingfinder.greetings.Greeting;
import tech.findanything.thingfinder.greetings.HelloMessage;
import tech.findanything.thingfinder.messages.CameraTransform;
import tech.findanything.thingfinder.messages.Detection;
import tech.findanything.thingfinder.messages.DetectionFrame;
import tech.findanything.thingfinder.messages.Detections;
import tech.findanything.thingfinder.messages.PointCloud;
import tech.findanything.thingfinder.messages.Transform;

@Controller
@EnableScheduling
@CrossOrigin(origins = "*")
public class GreetingController {

	private static final Logger log = LoggerFactory.getLogger(GreetingController.class);

	@MessageMapping("/hello")
	@SendTo("/topic/greetings")
	public Greeting greeting(HelloMessage message, MessageHeaders headers) throws Exception {

		log.info("Headers are {}", headers);
		log.info("Received greeting");
		
		Thread.sleep(1000);
		return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");

	}

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private DataService dataService;
	
	@Scheduled(fixedRate = 2000)
	public void timed() {

		log.debug("Sending scheduled message");
		String randomMessage = RandomStringUtils.randomAlphabetic(5);
		this.template.convertAndSend("/topic/greetings", new Greeting("Hello, " + HtmlUtils.htmlEscape(randomMessage) + "!"));
	}

//	@Scheduled(fixedRate = 2000)
//	public void sendDetections() {
//
////		log.debug("Sending detections");
////		Detections detection = dataService.getDetections();
////		if(points != null) this.template.convertAndSend("/get/detections", points);
//	}

	
//	Random rand = new Random();
//	points.getPoints().add(new Transform(rand.nextDouble() * 5, rand.nextDouble() * 5, rand.nextDouble() * 5));
	

	@Scheduled(fixedRate = 2000)
	public void sendDetections() {
		
		log.debug("Sending detections");
		Detections detections = new Detections(dataService.getDetections());
		if(detections != null) this.template.convertAndSend("/topic/get/detections", detections);
		
	}
	
	@Scheduled(fixedRate = 2000)
	public void sendPoints() {
		
		log.debug("Sending points");
		PointCloud points = dataService.getPoints();
		if(points != null) this.template.convertAndSend("/topic/get/points", points);
		
	}
	
	@Scheduled(fixedRate = 30)
	public void sendLocation() {
		
		log.debug("Sending location");
		CameraTransform cameraTransform = dataService.getCameraTransform();
		if(cameraTransform != null) this.template.convertAndSend("/topic/get/location", cameraTransform);
		
	}
	
	@MessageMapping("/give/points")
	public void points(PointCloud points) throws Exception {
		log.info("Received points");
		dataService.receivedPoints(points);
	}

	@MessageMapping("/give/reset")
	public void reset() throws Exception {
		log.info("Received reset request");
		dataService.reset();
	}
	
	@MessageMapping("/give/detection")
	public void detection(DetectionFrame detectionFrame) throws Exception {
		
//		log.info("Received detection {}", detectionFrame);
		
		if(detectionFrame.getImage() == null) return;
		
		dataService.receivedDetectionFrame(detectionFrame);

	}
	
	@MessageMapping("/give/location")
	public void location(CameraTransform cameraTransform) throws Exception {

//		log.info("Received location");
		dataService.receivedCameraTransform(cameraTransform);

	}

}
