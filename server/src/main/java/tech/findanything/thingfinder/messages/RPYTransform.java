package tech.findanything.thingfinder.messages;

public class RPYTransform {

	private double roll;
	private double pitch;
	private double yaw;

	public double getRoll() {
		return roll;
	}

	public double getPitch() {
		return pitch;
	}

	public double getYaw() {
		return yaw;
	}

	@Override
	public String toString() {
		return "RPYTransform [roll=" + roll + ", pitch=" + pitch + ", yaw=" + yaw + "]";
	}

	public RPYTransform(double roll, double pitch, double yaw) {
		super();
		this.roll = roll;
		this.pitch = pitch;
		this.yaw = yaw;
	}

	public RPYTransform() {
		super();
	}
	
	
	
}
