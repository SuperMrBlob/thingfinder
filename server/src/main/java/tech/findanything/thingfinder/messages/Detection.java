package tech.findanything.thingfinder.messages;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tech.findanything.thingfinder.Application;

public class Detection implements Comparable<Detection> {

	private String className;
	private double score;
	private Transform transform;
	private UUID id;
	private BoundingBox boundingBox;
	private long time;

	public String getClassName() {
		return className;
	}

	public double getScore() {
		return score;
	}

	public Transform getTransform() {
		return transform;
	}

	public UUID getId() {
		return id;
	}

	public UUID setId() {
		UUID id = Application.getUUID();
		this.setId(id);
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
	public BoundingBox getBoundingBox() {
		return boundingBox;
	}
	
	public long getTime() {
		return time;
	}
	
	public Detection() {
		super();
	}

	public Detection(String className, double score, Transform transform, UUID id) {
		super();
		this.className = className;
		this.score = score;
		this.transform = transform;
		this.id = id;
	}

	@Override
	public String toString() {
		return "Detection [className=" + className + ", score=" + score + ", transform=" + transform + ", id=" + id
				+ "]";
	}

	
	@Override
	public int compareTo(Detection o) {
		
	    int result = this.getClassName().compareTo(o.getClassName());
	    
	    if(result == 0){
	        return this.getId().compareTo(o.getId());
	    } else {
	    	return result;
	    }
	    
	}

}
