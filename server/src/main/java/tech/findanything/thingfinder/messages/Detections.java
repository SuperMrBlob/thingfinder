package tech.findanything.thingfinder.messages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class Detections {
	
	private ArrayList<Detection> detections;

	public Detections(Set<Detection> detections) {
		super();
		this.detections = new ArrayList<>(detections);
		Collections.sort(this.detections);
	}

	public Detections() {
		super();
	}

	public ArrayList<Detection> getDetections() {
		return detections;
	}
	
	
	
}
