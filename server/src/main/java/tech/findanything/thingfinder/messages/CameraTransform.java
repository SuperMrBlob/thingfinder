package tech.findanything.thingfinder.messages;

public class CameraTransform {

	private Transform position;
	private RPYTransform orientation;
	private double[] transformMatrix;
	
	public double[] getTransformMatrix() {
		return transformMatrix;
	}

	public Transform getPosition() {
		return position;
	}
	
	public RPYTransform getOrientation() {
		return orientation;
	}
	
	@Override
	public String toString() {
		return "CameraTransform [position=" + position + ", orientation=" + orientation + "]";
	}

	public CameraTransform() {
		super();
		this.position = new Transform(3, 5, 4);
		this.orientation = new RPYTransform(1, 1, 1);
	}	
	
}
