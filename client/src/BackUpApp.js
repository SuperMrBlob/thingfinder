import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { withStyles } from '@material-ui/core/styles';

import SendIcon from '@material-ui/icons/Send';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';

import Drawer from '@material-ui/core/Drawer';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import SockJsClient from 'react-stomp';

const drawerWidth = 240;
let sample_detections = {
    "detections": [
        {
            "className": "person",
            "score": 0.8500955700874329,
            "transform": {"x": 0.464070200920105, "y": 0.015222504734992981, "z": 0.2883283495903015},
            "id": "1bc94abe-15e5-4e5b-a1b7-bb1db8ddab2a"
        }, {
            "className": "bottle",
            "score": 0.8182314038276672,
            "transform": {"x": 0.5199549198150635, "y": -0.0826156884431839, "z": 0.1849818229675293},
            "id": "da3b05a8-81c3-4888-80a7-d8268ba10481"
        }, {
            "className": "potato",
            "score": 0.9,
            "transform": {"x": 3.0, "y": 3.0, "z": 3.0},
            "id": "4dee14dd-ec14-4f77-920e-09e46f01a022"
        }, {
            "className": "cup",
            "score": 0.9935280084609985,
            "transform": {"x": 0.26743075251579285, "y": -0.17995208501815796, "z": -0.36569464206695557},
            "id": "78d0f190-369e-48c0-bdd1-a8e6438dccc6"
        }, {
            "className": "laptop",
            "score": 0.9998670816421509,
            "transform": {"x": 0.019527986645698547, "y": -0.04272603988647461, "z": -0.04055022820830345},
            "id": "f4c76502-7492-48a1-8bc5-2d5ea6d5733e"
        }]
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100vh',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        position: 'absolute',
        zIndex: theme.zIndex.drawer + 1,
        // marginLeft: drawerWidth,
        // [theme.breakpoints.up('md')]: {
        //     width: `calc(100% - ${drawerWidth}px)`,
        // },
    },
    navIconHide: {
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            position: 'relative',
        },
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
    },

    spinner:{
        display: 'flex',
        height: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
    }
});


class App extends Component {

    objectList=(dictionary)=>{
        console.log(dictionary);
        let list = Object.keys(dictionary);


        let userList = list.map((item)=>{
            return(
                <ListItem button>
                    <ListItemText primary={dictionary[item].className} />
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Delete">
                            <SendIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            )
        });

        return(
            <List>
                {userList}
                <ListItem button>
                    <ListItemText primary={"wow"} />
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Delete">
                            <SendIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <ListItem button>
                    <ListItemText primary={"kasdf"} />
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Delete">
                            <SendIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            </List>
        )
    };

    render() {

        const {classes} = this.props;

        const drawer = (
            <div>
                <div className={classes.toolbar} />
                {this.objectList(sample_detections)}

                <Divider />

            </div>
        );

        return (
            <div className="App">

                <div>
                    <AppBar position="absolute">
                        <Toolbar>
                            <Typography variant="title" color="inherit" noWrap>
                                Clipped drawer
                            </Typography>
                        </Toolbar>
                    </AppBar>

                    <Drawer variant="permanent">
                        {drawer}
                    </Drawer>

                    <main>
                    </main>
                </div>

                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>

                <SockJsClient
                    url='http://10.77.1.122:8080/thingfinder'
                    topics={['/topic/get/points']}
                    onConnect={console.log("Connection established!")}
                    onDisconnect={console.log("Disconnected!")}
                    // onMessage={(msg) => { console.log("Points", msg); }}
                    // ref={ (client) => { this.clientRef = client }}
                    debug={true}
                />

                <SockJsClient
                    url='http://10.77.1.122:8080/thingfinder'
                    topics={['/topic/get/detections']}
                    onConnect={console.log("Connection established!")}
                    onDisconnect={console.log("Disconnected!")}
                    onMessage={(msg) => {
                        console.log("Detection", msg);
                    }}
                    // ref={ (client) => { this.clientRef = client }}
                    debug={true}
                />

            </div>
        );
    }
}

export default withStyles(styles)(App);
